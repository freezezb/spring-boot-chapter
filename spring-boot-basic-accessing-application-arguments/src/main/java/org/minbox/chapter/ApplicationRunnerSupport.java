package org.minbox.chapter;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * {@link ApplicationRunner} 实现类
 *
 * @author 恒宇少年
 */
@Component
public class ApplicationRunnerSupport implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("runner:");
        boolean isHaveSkip = args.containsOption("skip");
        System.out.println("skip：" + isHaveSkip);
        System.out.println(args.getOptionValues("skip"));
    }
}
